package PageEntry

import (
	"bytes"
	"io"
	"log"
	"math/rand"
	"os"

	randomdata "github.com/Pallinder/go-randomdata"

	"gitlab.com/remotejob/static_webs_utils/Makereadeble"
	"gitlab.com/remotejob/static_webs_utils/Mgenerator"
	"gitlab.com/remotejob/static_webs_utils/domains"
	"gitlab.com/remotejob/static_webs_utils/wordscount"
)

type EntryPage struct {
	EntryPage domains.Page
}

func NewEntryPage() *EntryPage {
	return &EntryPage{domains.Page{}}
}

func (newpage *EntryPage) AddContents(mfile string) {
	buf := bytes.NewBuffer(nil)

	f, err := os.Open(mfile)
	if err != nil {

		log.Println(err.Error())
	}
	_, err = io.Copy(buf, f)
	if err != nil {

		log.Println(err.Error())
	}

	err = f.Close()
	if err != nil {

		log.Println(err.Error())
	}
	mtext := Mgenerator.Generate(buf.Bytes())

	text := Makereadeble.Makehuman(mtext, 100, 200)

	newpage.EntryPage.Contents = text
}

func (newpage *EntryPage) AddTags(commonwords []string) {

	tags := wordscount.GetTags([]byte(newpage.EntryPage.Contents), commonwords, 10)

	newpage.EntryPage.Tags = tags

	// log.Println("tags", newpage.EntryPage.Tags)

}

func (newpage *EntryPage) AddKeywords(keywords []domains.Gkeywords) {

	numberstoshuffle := rand.Perm(len(keywords))

	var _keywords []string
	for i := 0; i < len(keywords); i++ {

		maxkeywordlen := len("слушать-и-скачать-зарубежную-музыку-бесплатно")

		// log.Println("max keyword len", maxkeywordlen)

		_keyword := keywords[numberstoshuffle[i]].Phrase

		if len(_keyword) < maxkeywordlen {
			_keywords = append(_keywords, keywords[numberstoshuffle[i]].Phrase)

			if len(_keywords) > 9 {
				break
			}

		} else {

			log.Println("To long ", _keyword)
		}
	}
	newpage.EntryPage.Keywords = _keywords
}
func (newpage *EntryPage) AddPaths() {

	var _paths []string

	for i := 0; i < 2; i++ {

		// log.Println(newpage.EntryPage.Keywords[i])
		_paths = append(_paths, newpage.EntryPage.Keywords[i])

	}

	newpage.EntryPage.Paths = _paths

}

func (newpage *EntryPage) AddTitle() {
	newpage.EntryPage.Title = newpage.EntryPage.Keywords[3]
}

func (newpage *EntryPage) AddAuthor() {

	authorName := randomdata.FullName(randomdata.RandomGender)

	newpage.EntryPage.Author = authorName

}

func (newpage *EntryPage) AddExternalLink(link string) {

	newpage.EntryPage.ExternalLink = link

}

func (newpage *EntryPage) AddImglLink(link string) {

	newpage.EntryPage.ImgLink = link

}
