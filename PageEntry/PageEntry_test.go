package PageEntry

import (
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/remotejob/static_webs_utils/dbhandler"

	"gitlab.com/remotejob/static_webs_utils/domains"
	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestEntryPage_AddContents(t *testing.T) {

	newPage := NewEntryPage()

	var _commonwords []string
	var _gkeywords []domains.Gkeywords
	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	} else {

		_commonwords = dbhandler.GetCommonWords(*dbsession, "realestate", "ru_RU")
		_gkeywords = dbhandler.GetAllGkeywords(*dbsession, "realestate", "ru_RU")

	}

	newPage.AddContents("/tmp/realestate_ru_RU.txt")
	newPage.AddTags(_commonwords)
	newPage.AddKeywords(_gkeywords)
	newPage.AddPaths()
	newPage.AddTitle()
	newPage.AddAuthor()
	newPage.AddExternalLink("http://www.mazurov.eu")

	log.Println(newPage)

}
