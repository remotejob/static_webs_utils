package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/remotejob/comutils/gen"

	"gitlab.com/remotejob/static_webs_utils/CheckIfSiteExist"
	"gitlab.com/remotejob/static_webs_utils/CreateNewSite"
	"gitlab.com/remotejob/static_webs_utils/CreatePage"
	"gitlab.com/remotejob/static_webs_utils/FilterDomain"
	"gitlab.com/remotejob/static_webs_utils/PageEntry"
	"gitlab.com/remotejob/static_webs_utils/bookgen"
	"gitlab.com/remotejob/static_webs_utils/checkMfileSize"
	"gitlab.com/remotejob/static_webs_utils/dbhandler"
	"gitlab.com/remotejob/static_webs_utils/domains"
	"gitlab.com/remotejob/static_webs_utils/getSitesList"

	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string
var maxfilesize int

var _commonwords []string
var _gkeywords []domains.Gkeywords

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")
	maxfilesize, _ = strconv.Atoi(os.Getenv("MAXFILESIZE"))

	mongoDBDialInfo = mgo.DialInfo{

		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func main() {

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	sites := getSitesList.GetList("sites.csv")

	for _, site := range sites {

		log.Println("Elab", site.Domain, site.Host)
		_commonwords = dbhandler.GetCommonWords(*dbsession, site.Theme, site.Locale)
		_gkeywords = dbhandler.GetAllGkeywords(*dbsession, site.Theme, site.Locale)

		mfile := "/tmp/" + site.Theme + "_" + site.Locale + ".txt"

		bookgen.Create(_gkeywords, mfile)

		_domain, err := CheckIfSiteExist.Check(*dbsession, site)

		if err != nil {

			log.Println(err.Error())

			if err.Error() == "not found" {
				CreateNewSite.Create(*dbsession, site)
			} else {

				panic(err.Error())
			}

		} else {

			if _domain.Domain != "" {
				_filter := FilterDomain.Filter(_domain, site)

				newPage := PageEntry.NewEntryPage()

				newPage.AddContents(mfile)
				newPage.AddTags(_commonwords)
				newPage.AddKeywords(_gkeywords)
				newPage.AddPaths()
				newPage.AddTitle()
				newPage.AddAuthor() // log.Println("_domaind.Domain", _domain.Domain)
				newPage.AddExternalLink("")
				newPage.AddImglLink("")
				backtime := gen.Random(0, 5000000)
				now := time.Now()
				sitemaptime := now.Add(time.Duration(-backtime) * time.Second)

				_newPage := domains.Page{now, now, sitemaptime, newPage.EntryPage.Author, newPage.EntryPage.Keywords, newPage.EntryPage.Tags, newPage.EntryPage.Paths, newPage.EntryPage.Title, newPage.EntryPage.Contents, newPage.EntryPage.ExternalLink, newPage.EntryPage.ImgLink}
				// log.Println("newPAGE", _newPage)
				CreatePage.Create(*dbsession, _domain, _filter, _newPage)

			} else {

				log.Println("Obj empty new page next loop")
			}

		}

		checkMfileSize.Check(mfile, maxfilesize)

	}
}
