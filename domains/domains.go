package domains

import "time"

// type HugoFrontMatter struct {
// 	Title       string
// 	Description string
// 	Date        time.Time
// 	Tags        []string
// 	Categories  []string
// }

//Commonwords Commonwords
type Commonwords struct {
	Theme  string
	Locale string
	Rating float64
	Word   string
}

//Page web page
type Page struct {
	Created      time.Time
	Updated      time.Time
	Sitemapdate  time.Time
	Author       string
	Keywords     []string
	Tags         []string
	Paths        []string
	Title        string
	Contents     string
	ExternalLink string
	ImgLink      string
}

//Locale define language
type Locale struct {
	Language string
	Pages    []Page
}

//Theme web theme
type Theme struct {
	Theme   string
	Locales []Locale
}

//Host web host
type Host struct {
	Host    string
	Variant string
	Themes  []Theme
}

//Domain web domain
type Domain struct {
	Domain    string
	Analytics string
	Hosts     []Host
}

// Gphrase comment
type Gkeywords struct {
	Phrase string `bson:"Phrase"`
	Rating int    `bson:"Rating"`
}

type Site struct {
	Domain    string
	Host      string
	Variant   string
	Theme     string
	Locale    string
	Analytics string
	Rootdir   string
	Tax0      string
	Tax1      string
}
