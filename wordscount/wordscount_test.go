package wordscount

import (
	"bytes"
	"io"
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/remotejob/static_webs_utils/dbhandler"
	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestGetTags(t *testing.T) {
	type args struct {
		bfile       []byte
		commonwords []string
		quant       int
	}

	buf := bytes.NewBuffer(nil)

	f, err := os.Open("/tmp/realestate_ru_RU.txt")
	if err != nil {

		log.Println(err.Error())
	}
	_, err = io.Copy(buf, f)
	if err != nil {

		log.Println(err.Error())
	}

	err = f.Close()
	if err != nil {

		log.Println(err.Error())
	}
	var _commonwords []string
	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	} else {

		_commonwords = dbhandler.GetCommonWords(*dbsession, "realestate", "ru_RU")

	}
	tests := []struct {
		name string
		args args
		// want []string
	}{
		// TODO: Add test cases.
		{"test0", args{buf.Bytes(), _commonwords, 50}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// if got := GetTags(tt.args.bfile, tt.args.commonwords, tt.args.quant); !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("GetTags() = %v, want %v", got, tt.want)
			// }

			got := GetTags(tt.args.bfile, tt.args.commonwords, tt.args.quant)
			log.Println(got)

		})
	}
}
