package CreateNewSite

import (
	"log"

	"gitlab.com/remotejob/static_webs_utils/domains"
	mgo "gopkg.in/mgo.v2"
)

// func Create(dbsession mgo.Session, domain string, host string, variant string, theme string, locale string, analytics string) {
func Create(dbsession mgo.Session, site domains.Site) {

	dbsession.SetMode(mgo.Monotonic, true)

	c := dbsession.DB("static_webs").C("sites")

	var pages []domains.Page

	_locale := domains.Locale{site.Locale, pages}
	var locales []domains.Locale
	locales = append(locales, _locale)

	_theme := domains.Theme{site.Theme, locales}
	var themes []domains.Theme
	themes = append(themes, _theme)

	_host := domains.Host{site.Host, site.Variant, themes}
	var hosts []domains.Host
	hosts = append(hosts, _host)

	_domain := domains.Domain{site.Domain, site.Analytics, hosts}

	err := c.Insert(_domain)
	if err != nil {
		log.Fatal(err)
	}

}
