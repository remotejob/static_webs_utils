package CreateNewSite

import (
	"log"
	"os"
	"testing"
	"time"

	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestCreate(t *testing.T) {
	type args struct {
		dbsession mgo.Session
		domain    string
		host      string
		variant   string
		theme     string
		locale    string
		analytics string
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0", args{*dbsession, "kvartira-tsentr.eu", "www", "variant0", "realestate", "ru_RU", "analit-1122121"}},
	}
	for _, tt := range tests {
		Create(tt.args.dbsession, tt.args.domain, tt.args.host, tt.args.variant, tt.args.theme, tt.args.locale, tt.args.analytics)
	}
}
