package createMdPage

import (
	"bufio"
	"bytes"
	"encoding/json"
	"log"
	"os"

	"gitlab.com/remotejob/static_webs_utils/domains"

	"strings"
)

func CreateMd(mdfile string, page domains.Page, tax0 string, tax1 string) {

	var categories []string

	for i := 5; i < 10; i++ {

		categories = append(categories, page.Keywords[i])

	}

	myData := make(map[string]interface{})

	myData["Title"] = strings.Title(page.Title)
	myData["Description"] = page.Keywords[4]
	myData["Description"] = page.Keywords[4]
	myData["Date"] = page.Sitemapdate
	myData[tax0] = page.Tags
	myData[tax1] = categories

	frontMatterJson, _ := json.Marshal(myData)

	// log.Println(jsonPrettyPrint(string(frontMatterJson)))

	f, err := os.Create(mdfile)
	if err != nil {

		log.Panicln(err)
	}

	strToWrite := jsonPrettyPrint(string(frontMatterJson)) + " " + page.Contents

	w := bufio.NewWriter(f)
	_, err = w.WriteString(string(strToWrite))
	if err != nil {

		log.Panicln(err)
	}

	w.Flush()

}

func jsonPrettyPrint(in string) string {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", "\t")
	if err != nil {
		return in
	}
	return out.String()
}
