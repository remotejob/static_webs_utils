package getSitesList

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"

	"gitlab.com/remotejob/static_webs_utils/domains"
)

func GetList(file string) []domains.Site {

	var sites []domains.Site

	fcsv, err := os.Open(file)

	if err != nil {

		panic(err.Error())
	} else {

		r := csv.NewReader(bufio.NewReader(fcsv))
		r.Comma = ','
		// set_commonwords := make(map[string]struct{})

		for {
			record, err := r.Read()

			if err == io.EOF {
				break
			}

			// log.Println(record[2])

			site := domains.Site{record[0], record[1], record[2], record[3], record[4], record[5], record[6], record[7], record[8]}

			sites = append(sites, site)

		}

	}
	return sites
}
