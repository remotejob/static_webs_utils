package CreateDomainObj

import "gitlab.com/remotejob/static_webs_utils/domains"

func CreateObj(domain string, host string, variant string, theme string, locale string) domains.Domain {

	var pages []domains.Page
	// pages = append(pages, page)

	_locale := domains.Locale{locale, pages}
	var locales []domains.Locale
	locales = append(locales, _locale)

	_theme := domains.Theme{theme, locales}
	var themes []domains.Theme
	themes = append(themes, _theme)

	_host := domains.Host{host, variant, themes}
	var hosts []domains.Host
	hosts = append(hosts, _host)

	_domain := domains.Domain{domain, hosts}

	return _domain

}
