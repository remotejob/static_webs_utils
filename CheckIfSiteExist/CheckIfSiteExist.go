package CheckIfSiteExist

import (
	"log"

	"gitlab.com/remotejob/static_webs_utils/domains"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Check if site exist
// func Check(dbsession mgo.Session, domain string, host string, variant string, theme string, locale string) (domains.Domain, error) {
func Check(dbsession mgo.Session, site domains.Site) (domains.Domain, error) {

	var err error
	dbsession.SetMode(mgo.Monotonic, true)

	c := dbsession.DB("static_webs").C("sites")

	result := domains.Domain{}

	var ret domains.Domain

	searchQ := bson.M{"domain": site.Domain}

	err = c.Find(searchQ).One(&result)

	_hosts := result.Hosts

	for _, _host := range _hosts {

		if _host.Host == site.Host {

			for _, _theme := range _host.Themes {

				if _theme.Theme == site.Theme {

					for _, _locale := range _theme.Locales {

						if _locale.Language == site.Locale {

							ret = result

						}

					}

				}

			}

		}

	}

	if ret.Domain == "" && result.Domain != "" {
		log.Println("host ", site.Host, "Don't EXIST create!")

		var pages []domains.Page

		_locale := domains.Locale{site.Locale, pages}
		var locales []domains.Locale
		locales = append(locales, _locale)

		_theme := domains.Theme{site.Theme, locales}
		var themes []domains.Theme
		themes = append(themes, _theme)

		_host := domains.Host{site.Host, site.Variant, themes}

		result.Hosts = append(result.Hosts, _host)

		err = c.Update(searchQ, result)
		if err != nil {
			panic(err)
		}
	}

	return ret, err
}
