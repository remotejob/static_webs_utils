package bookgen

import (
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/remotejob/static_webs_utils/domains"
)

//Create create mfile
func Create(gkeywords []domains.Gkeywords, filename string) {

	log.Println("Start Create file", filename)

	// gkeywords := dbhandler.GetAllGkeywords(session, strings.TrimSpace(themes), strings.TrimSpace(locale))

	var numberstoshuffle []int

	rand.Seed(time.Now().UTC().UnixNano())

	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {

		log.Println(err.Error())
	}

	defer f.Close()

	numberstoshuffle = rand.Perm(len(gkeywords))

	for _, i := range numberstoshuffle {

		paragraph := gkeywords[i].Phrase + "\n"

		if _, err = f.WriteString(paragraph); err != nil {

			log.Println(err.Error())
		}
	}
	err = f.Close()
	if err != nil {
		log.Println(err.Error())
	}
}
