package bookgen

import (
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/remotejob/static_webs_utils/dbhandler"

	"gitlab.com/remotejob/static_webs_utils/domains"
	"gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestCreate(t *testing.T) {
	type args struct {
		keywords []domains.Gkeywords
		filename string
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}
	gkeywords := dbhandler.GetAllGkeywords(*dbsession, "realestate", "ru_RU")
	tests := []struct {
		name string
		args args
	}{

		{"test0", args{gkeywords, "/tmp/realestate_ru_RU.txt"}},
	}
	for _, tt := range tests {
		Create(tt.args.keywords, tt.args.filename)
	}
}
