package commonwords_utils

import (
	"log"
	"os"
	"testing"
	"time"

	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestUpload(t *testing.T) {
	type args struct {
		dbsession mgo.Session
		file      string
		theme     string
		locale    string
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	defer dbsession.Close()
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0", args{*dbsession, "/home/juno/gowork/src/gitlab.com/remotejob/static_webs_utils/ru425360.txt", "realestate", "ru_RU"}},
		// {"test0", args{*dbsession, "/home/juno/gowork/src/gitlab.com/remotejob/static_webs_utils/fi500000.txt", "fortune", "fi_FI"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Upload(tt.args.dbsession, tt.args.file, tt.args.theme, tt.args.locale)
		})
	}
}
