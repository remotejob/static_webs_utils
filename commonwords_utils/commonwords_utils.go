package commonwords_utils

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"

	mgo "gopkg.in/mgo.v2"

	"gitlab.com/remotejob/static_webs_utils/dbhandler"
	"gitlab.com/remotejob/static_webs_utils/domains"
)

func Upload(dbsession mgo.Session, file string, theme string, locale string) {

	fcsv, _ := os.Open(file)

	r := csv.NewReader(bufio.NewReader(fcsv))
	r.Comma = ' '
	// set_commonwords := make(map[string]struct{})
	var commonwords []domains.Commonwords
	// var commonwords_limited []domains.Commonwords

	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}

		// log.Println(record[0])
		// Decode(theme, locale, record)

		rating, err := strconv.ParseFloat(record[1], 64)
		// rating := record[1]
		if err == nil {
			_wordOBj := domains.Commonwords{theme, locale, rating, record[0]}

			commonwords = append(commonwords, _wordOBj)

		}

	}

	log.Println("commonwords len", len(commonwords))

	dbhandler.UploadCommonWords(dbsession, commonwords)

	// var counter int = 0

	// for i, word := range commonwords {

	// 	counter++
	// 	log.Println("count", counter)
	// 	commonwords_limited = append(commonwords_limited, word)

	// 	if counter == 400000 {

	// 		dbhandler.UploadCommonWords(dbsession, commonwords_limited)

	// 		counter = 0
	// 		commonwords_limited = commonwords_limited[:0]

	// 		// time.Sleep(30000 * time.Millisecond)
	// 	} else {

	// 		if i == len(commonwords) {

	// 			dbhandler.UploadCommonWords(dbsession, commonwords_limited)

	// 			counter = 0
	// 			commonwords_limited = commonwords_limited[:0]

	// 		}

	// 	}

	// }

}
