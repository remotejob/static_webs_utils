package Makereadeble

import (
	"strings"

	"github.com/remotejob/comutils/gen"
	"github.com/remotejob/comutils/str"
)

func Makehuman(mtext string, minlen int, maxlen int) string {
	mtexttokens := strings.Fields(mtext)
	q := gen.Random(minlen, maxlen)
	var tmpline string
	var result string
	for i, token := range mtexttokens {

		if i == 0 {
			tmpline = token
		} else {

			tmpline = tmpline + " " + token

		}
		if len(tmpline) > q {
			result = result + " " + str.UpcaseInitial(tmpline) + "."
			tmpline = ""
			q = gen.Random(minlen, maxlen)

		}

	}
	return strings.TrimSpace(result)
}
