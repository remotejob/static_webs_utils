package CreateHostObj

import "gitlab.com/remotejob/static_webs_utils/domains"

func CreateHost(host string, theme string, locale string, variant string) domains.Host {

	var pages []domains.Page
	// pages = append(pages, page)

	_locale := domains.Locale{locale, pages}
	var locales []domains.Locale
	locales = append(locales, _locale)

	_theme := domains.Theme{theme, locales}
	var themes []domains.Theme
	themes = append(themes, _theme)

	_host := domains.Host{host, variant, themes}

	return _host

}
