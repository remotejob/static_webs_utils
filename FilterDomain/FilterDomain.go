package FilterDomain

import "gitlab.com/remotejob/static_webs_utils/domains"

// func Filter(domainObj domains.Domain, domain string, host string,  variant string,theme string, locale string) []int {
func Filter(domainObj domains.Domain, site domains.Site) []int {

	var hostKey int
	var themeKey int
	var localeKey int

	_hosts := domainObj.Hosts

	for i, _host := range _hosts {

		if _host.Host == site.Host {
			hostKey = i
			for i, _theme := range _host.Themes {

				if _theme.Theme == site.Theme {
					themeKey = i
					for i, _locale := range _theme.Locales {

						if _locale.Language == site.Locale {

							localeKey = i

						}

					}

				}

			}

		}
	}

	var keys []int
	keys = append(keys, hostKey, themeKey, localeKey)

	return keys
}
