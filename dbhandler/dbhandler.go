package dbhandler

import (
	"log"

	"time"

	"gitlab.com/remotejob/static_webs_utils/domains"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetAllGkeywords(session mgo.Session, themes string, locale string) []domains.Gkeywords {

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("gkeywords").C("keywords")

	var results []domains.Gkeywords

	err := c.Find(bson.M{"Themes": themes, "Locale": locale}).Select(bson.M{"Phrase": 1, "Rating": 1}).All(&results)
	if err != nil {

		log.Fatal(err)
	}

	return results
}

func UploadCommonWords(session mgo.Session, words []domains.Commonwords) {
	session.SetMode(mgo.Monotonic, true)
	session.SetSocketTimeout(1 * time.Hour)
	session.SetSyncTimeout(1 * time.Hour)

	c := session.DB("commonwords").C("words")

	blk := c.Bulk()
	blk.Unordered()

	// var counter int = 0

	for _, word := range words {

		// c.Insert(word)
		blk.Insert(word)
		// counter++

		// if counter == 10000 {

		// time.Sleep(5000 * time.Millisecond)
		// res, err := blk.Run()
		// if err != nil {

		// 	err.Error()
		// } else {

		// 	log.Println("res", res)

		// }
		// blk.RemoveAll()
		// counter = 0

	}

	res, err := blk.Run()
	if err != nil {

		err.Error()
	} else {

		log.Println(res)

	}

}

func GetCommonWords(session mgo.Session, themes string, locale string) []string {
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("commonwords").C("words")

	var results []string

	var _commonwords []domains.Commonwords

	err := c.Find(bson.M{"theme": themes, "locale": locale}).Select(bson.M{"word": 1}).All(&_commonwords)
	if err != nil {

		log.Fatal(err)
	}

	for _, cword := range _commonwords {

		results = append(results, cword.Word)
	}

	return results
}
