package checkMfileSize

import (
	"log"
	"os"
)

func Check(mfile string, maxfilesize int) {
	var markfileSize int64
	f, err := os.Open(mfile)
	if err != nil {

		log.Println(err.Error())
	} else {
		fi, err := f.Stat()
		if err != nil {
			log.Fatal(err)
		} else {
			// log.Println("/mfile  size", fi.Size())
			markfileSize = fi.Size()
		}
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println(mfile, "size", fi.Size())
		markfileSize = fi.Size()
	}

	if markfileSize > int64(maxfilesize) {
		log.Println("Time delete markfile max permited", maxfilesize)
		err = os.Remove(mfile)
		if err != nil {
			log.Fatal(err)
		}
	}
}
