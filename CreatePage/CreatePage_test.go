package CreatePage

import (
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/remotejob/static_webs_utils/CheckIfSiteExist"
	"gitlab.com/remotejob/static_webs_utils/FilterDomain"
	"gitlab.com/remotejob/static_webs_utils/domains"
	mgo "gopkg.in/mgo.v2"
)

var addrs []string
var dbadmin string
var username string
var password string
var mechanism string

var mongoDBDialInfo mgo.DialInfo

func init() {
	addrs = []string{os.Getenv("ADDRS")}
	dbadmin = os.Getenv("DBADMIN")
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")
	mechanism = os.Getenv("MECHANISM")

	mongoDBDialInfo = mgo.DialInfo{
		Addrs:     addrs,
		Timeout:   60 * time.Second,
		Database:  dbadmin,
		Username:  username,
		Password:  password,
		Mechanism: mechanism,
	}
}
func TestCreate(t *testing.T) {
	type args struct {
		dbsession mgo.Session
		domain    domains.Domain
		filter    []int
		page      domains.Page
	}

	dbsession, err := mgo.DialWithInfo(&mongoDBDialInfo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	_domain, err := CheckIfSiteExist.Check(*dbsession, "kvartira-tsentr.eu", "blog", "variant0", "realestate", "ru_RU")
	now := time.Now()
	_page := domains.Page{now, now, now, "BILL", []string{"key22", "key2222"}, []string{"tags22", "tags222"}, []string{"path2", "path2"}, "Title2", "LSLSLSLSLSLSLS", "http://test.com"}

	_filter := FilterDomain.Filter(_domain, "kvartira-tsentr.eu", "blog", "variant0", "realestate", "ru_RU")

	log.Println("_filter", _filter)

	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0", args{*dbsession, _domain, _filter, _page}},
	}
	for _, tt := range tests {
		Create(tt.args.dbsession, tt.args.domain, tt.args.filter, tt.args.page)
	}
}
