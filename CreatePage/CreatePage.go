package CreatePage

import (
	"gitlab.com/remotejob/static_webs_utils/domains"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Create webpage
func Create(dbsession mgo.Session, domain domains.Domain, filter []int, page domains.Page) {

	domain.Hosts[filter[0]].Themes[filter[1]].Locales[filter[2]].Pages = append(domain.Hosts[filter[0]].Themes[filter[1]].Locales[filter[2]].Pages, page)

	// log.Println(domain)

	dbsession.SetMode(mgo.Monotonic, true)

	c := dbsession.DB("static_webs").C("sites")

	searchQ := bson.M{"domain": domain.Domain}

	err := c.Update(searchQ, domain)
	if err != nil {
		panic(err)
	}

}
